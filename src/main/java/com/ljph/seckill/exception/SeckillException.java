package com.ljph.seckill.exception;

/**
 * 秒杀相关业务异常
 * Created by yuzhou on 16/8/27.
 */
public class SeckillException extends RuntimeException {

    public SeckillException(String message) {
        super(message);
    }

    public SeckillException(String message, Throwable cause) {
        super(message, cause);
    }
}

